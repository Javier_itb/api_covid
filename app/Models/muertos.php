<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class muertos extends Model
{
    use HasFactory;
    protected $table = 'muertos';
    public $timestamps = false;
    protected $fillable = ['fecha','id_ccaa','incidencia'];
}
