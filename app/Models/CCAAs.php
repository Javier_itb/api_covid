<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CCAAs extends Model
{
    use HasFactory;
    protected $table = 'ccaas';
    public $timestamps = false;
    public function ia14() {
        $this->hasMany('App\Models\ia14');
    }
}
