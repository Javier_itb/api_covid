<?php

namespace App\Http\Resources;

use App\Models\CCAAs;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class ShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $pais = DB::table('paises')->join('ccaas','paises.id','=','ccaas.id_pais')->select('paises.*')->first();
        $ccaa = CCAAs::where('id', $this->id_ccaa)->first();


        $path = $request->getPathInfo();
        $casos = substr_count($path, 'casos',0,strlen($path));
        $muertos = substr_count($path, 'muertos',0,strlen($path));

        if ($casos > 0 || $muertos > 0) {
            return [
                'fecha' => $this->fecha,
                'ccaa' => $ccaa->nombre,
                'pais' => $pais->nombre,
                'value' => $this->numero,
            ];
        } else {
            return  [
                'fecha' => $this->fecha,
                'ccaa' => $ccaa->nombre,
                'pais' => $pais->nombre,
                'value' => $this->incidencia,
            ];
        }
    }
}
