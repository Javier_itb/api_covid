<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\casos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CasosController extends Controller {
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $casos = new casos();

        $casos->fecha = $request->fecha;
        $casos->id_ccaa = $request->id_ccaa;
        $casos->numero = $request->numero;
        $casos->save();
        return response()->json($casos);
    }

    public function showAll()
    {
        $casos = casos::all();
        if (!$casos) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No hay datos'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$casos],200);
    }

    public function showCollection($fecha, $fecha1)
    {
        if ($fecha > $fecha1) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Primera fecha mayor que la segunda'])],404);
        }

        $casos = DB::select(DB::raw("select * from casos where fecha BETWEEN '$fecha' and '$fecha1' "));

        if (!$casos) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra la fecha'])],404);
        }

        return new CovidCollection($casos);
    }

    public function show($id)
    {
        //$ia14 = DB::select(DB::raw("select * from ia14 where fecha = '$id'"));
        $casos = casos::where('fecha', $id)->first();
        if (!$casos) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia14],200);
        return new ShowResource($casos);
    }
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}

