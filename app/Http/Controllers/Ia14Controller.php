<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\ia14;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ia14Controller extends Controller {
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $ia14 = new ia14();

        $ia14->fecha = $request->fecha;
        $ia14->id_ccaa = $request->id_ccaa;
        $ia14->incidencia = $request->incidencia;
        $ia14->save();
        return response()->json($ia14);
    }

    public function showAll()
    {
        $ia14 = ia14::all();
        if (!$ia14) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No hay datos'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$ia14],200);
    }

    public function showCollection($fecha, $fecha1)
    {
        if ($fecha > $fecha1) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Primera fecha mayor que la segunda'])],404);
        }

        $ia14 = DB::select(DB::raw("select * from ia14 where fecha BETWEEN '$fecha' and '$fecha1' "));

        if (!$ia14) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra la fecha'])],404);
        }

        return new CovidCollection($ia14);
    }

    public function show($id)
    {
        //$ia14 = DB::select(DB::raw("select * from ia14 where fecha = '$id'"));
        $ia14 = ia14::where('fecha', $id)->first();
        if (!$ia14) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia14],200);
        return new ShowResource($ia14);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
