<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\muertos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MuertosController extends Controller
{
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $muertos = new muertos();

        $muertos->id_ccaa = $request->id_ccaa;
        $muertos->numero = $request->numero;
        $muertos->fecha = $request->fecha;
        $muertos->save();
        return response()->json($muertos);
    }

    public function showAll()
    {
        $muertos = muertos::all();
        if (!$muertos) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No hay datos'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$muertos],200);
    }

    public function showCollection($fecha, $fecha1)
    {
        if ($fecha > $fecha1) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Primera fecha mayor que la segunda'])],404);
        }

        $muertos = DB::select(DB::raw("select * from muertos where fecha BETWEEN '$fecha' and '$fecha1' "));

        if (!$muertos) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra la fecha'])],404);
        }

        return new CovidCollection($muertos);
    }

    public function show($id)
    {
        //$ia14 = DB::select(DB::raw("select * from ia14 where fecha = '$id'"));
        $muertos = muertos::where('fecha', $id)->first();
        if (!$muertos) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia14],200);
        return new ShowResource($muertos);
    }
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
