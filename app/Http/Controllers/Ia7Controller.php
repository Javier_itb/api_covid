<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\ia7;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ia7Controller extends Controller {
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $ia7 = new ia7();

        $ia7->fecha = $request->fecha;
        $ia7->id_ccaa = $request->id_ccaa;
        $ia7->incidencia = $request->incidencia;
        $ia7->save();
        return response()->json($ia7);
    }

    public function showAll()
    {
        $ia7 = ia7::all();
        if (!$ia7) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No hay datos'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$ia7],200);
    }

    public function showCollection($fecha, $fecha1)
    {
        if ($fecha > $fecha1) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Primera fecha mayor que la segunda'])],404);
        }

        $ia7 = DB::select(DB::raw("select * from ia7 where fecha BETWEEN '$fecha' and '$fecha1' "));

        if (!$ia7) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra la fecha'])],404);
        }

        return new CovidCollection($ia7);
    }

    public function show($id)
    {
        //$ia14 = DB::select(DB::raw("select * from ia14 where fecha = '$id'"));
        $ia7 = ia7::where('fecha', $id)->first();
        if (!$ia7) {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia14],200);
        return new ShowResource($ia7);
    }
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
