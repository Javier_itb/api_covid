#!/bin/bash

for i in $(cat dades.csv)
do
fecha=$(echo $i | cut -d';' -f1)
casos=$(echo $i | cut -d';' -f3)
muertos=$(echo $i | cut -d';' -f7)
ia14=$(echo $i | cut -d';' -f5)
ia7=$(echo $i | cut -d';' -f6)



echo "INSERT INTO ia14 (fecha, id_ccaa, incidencia) VALUES ('$fecha',1,$ia14);"
echo "INSERT INTO ia7 (fecha, id_ccaa, incidencia) VALUES ('$fecha',1,$ia7);"
echo "INSERT INTO casos (fecha, id_ccaa, numero) VALUES ('$fecha',1,$casos);"
echo "INSERT INTO muertos (fecha, id_ccaa, numero) VALUES ('$fecha',1,$muertos);"

done
